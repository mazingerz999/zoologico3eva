<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//         Crearemos un nuevo seeder llamado UserSeeder y lo completaremos para crear un par de
// usuarios de prueba. Recuerda que para guardar el password es necesario encriptarlo
// manualmente usando el método bcrypt.
// Por último, tendremos que ejecutar el comando de Artisan que procesa las semillas.
$a = new User();
$a->name = "Alberto";
 $a->email = "josealberto.lavingonzalez@iesmiguelherrero.com";
 $a->password = bcrypt("alberto");
 $a->save();

$b = new User();
$b->name = "Carlos";
 $b->email = "carlos@iesmiguelherrero.com";
 $b->password = bcrypt("carlos");
 $b->save();
 $this->command->info('Tabla Usuarios inicializada con datos');

    }
}
