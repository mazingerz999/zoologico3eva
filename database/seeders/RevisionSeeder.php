<?php

namespace Database\Seeders;

use App\Models\Animal;
use App\Models\Revision as ModelsRevision;
use Illuminate\Database\Seeder;
use App\Models\Revision;


class RevisionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $revision1=new Revision();
        $revision1->animal_id=Animal::first()->id;
        $revision1->fecha="2012/05/05";
        $revision1->descripcion="Se le hizo la revision y todo era correcto";

        $revision2=new Revision();
        $revision2->animal_id=Animal::first()->id;
        $revision2->fecha="2018/05/05";
        $revision2->descripcion="Se le hizo la revision y parecia estar en mal estado";

        $revision1->save();
        $revision2->save();
    }
}
