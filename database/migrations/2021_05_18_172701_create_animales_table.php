<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnimalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animales', function (Blueprint $table) {
            $table->id();
            $table->string('especie');
            $table->string('slug')->unique();
            $table->double('peso',6,1);
            $table->double('altura',6,1);
            $table->date('fechaNacimiento');
            $table->string('imagen', null);
            $table->string('alimentacion', null);
            $table->longText('descripcion', null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animales');
    }
}
