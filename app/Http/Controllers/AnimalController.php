<?php

namespace App\Http\Controllers;

use App\Models\Animal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

use function PHPUnit\Framework\isEmpty;

class AnimalController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $animales=Animal::all();

       // return view('animales.index', compact('animales'));
        return view('animales.index', ["animales"=>$animales]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view("animales.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        /* En el método store creamos una nueva instancia del modelo Animal, asignamos el
    valor de todos los campos de entrada y los guardamos. Por último, después de
    guardar, hacemos una redirección a la ruta animales.show .*/
    $animal=new Animal();
    $animal->especie=$request->especie;
        $animal->peso=$request->peso;
        $animal->altura=$request->altura;
        $animal->fechaNacimiento=$request->fechaNacimiento;
        $animal->imagen=$request->imagen->store('images');
        if (!empty($request->imagen) && $request->imagen->isValid()) {
            $animal->imagen=$request->imagen->store('', 'animales');
        }
        $animal->alimentacion=$request->alimentacion;
        $animal->descripcion=$request->descripcion;
        $animal->slug = Str::slug($animal->especie);
        $animal->save();
        return redirect()->route("animales.show", $animal);
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    }
    public function show(Animal $animal)
    {
        //

        return view('animales.show', ['animal'=>$animal]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Animal $animal)
    {
        //

        return view('animales.edit', ['animal'=>$animal]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Animal $animal)
    {
    /*
    • En el método update recibe el animal por parámetro y actualizamos sus campos y los
    guardamos. Por último, realizamos una redirección a la pantalla con la vista detalle del
    animal editado.*/

        $animal->especie=$request->especie;
        $animal->peso=$request->peso;
        $animal->altura=$request->altura;
        $animal->fechaNacimiento=$request->fechaNacimiento;
    if (!empty($request->imagen) && $request->imagen->isValid()) {

        //ESTO BORRA LA IMAGEN Y LA SUBE DE NUEVO

            Storage::disk('animales')->delete($animal->imagen);
            $animal->imagen=$request->imagen->store('', 'animales');

        /////

        }
        $animal->alimentacion=$request->alimentacion;
        $animal->descripcion=$request->descripcion;
        $animal->slug = Str::slug($animal->especie);

        return view('animales.show', ['animal'=>$animal])->with("mensaje", "Ha habido un error");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

