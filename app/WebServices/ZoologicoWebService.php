<?php

use App\Models\Animal;
use App\Models\User;

class ZoologicoWebService {

public function login($user, $password){
    # code...

    $encontrado=User::where('email', $user)->where('password', $password)->get();

    if ($encontrado!=null) {
        # code...
        return true;
    }else{
        return false;
    }
}
public function getAnimal($id){
    # code...
    $animal=Animal::find($id);

    return $animal;

}
public function getAnimales(){
    # code...
    $all=[];
    $all=Animal::all();
    return $all;
}
public function getAnimalesAlimentacion($alimentacion){
    # code...
    $comidas=[];
    $comidas=Animal::where('alimentacion', $alimentacion)->get();

    return $comidas;

}
public function busqueda($especie){
    # code...
    $search=[];
    $search=Animal::where('especie', 'like', '%'.$especie.'%')->get();


    return $search;
}

}
