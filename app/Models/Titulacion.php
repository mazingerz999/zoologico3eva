<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Titulacion extends Model
{
    use HasFactory;

    protected $table="titulaciones";
    protected $guarded=[];


   public function cuidadores()
    {
        # code...
        return $this->hasMany(Cuidador::class, 'id_titulacion1')->orWhere('id_titulacion2', $this->id);
    }
    public function getRouteKeyName()
    {
        # code...
        return "slug";
    }

}
