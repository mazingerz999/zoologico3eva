<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Animal extends Model
{
    use HasFactory;

    protected $table="animales";
    protected $guarded=[];

    public function getEdad(){
 $fechaFormateada=Carbon::parse($this->fechaNacimiento);
 return $fechaFormateada->diffInYears(Carbon::now());
    }

   public function revisiones()
    {
        # code...
        return $this->hasMany(Revision::class);
    }
   public function cuidadores()
    {
        # code...
        return $this->belongsToMany(Cuidador::class);
    }

   public function getRouteKeyName()
    {
        # code...
        return "slug";
    }

}
