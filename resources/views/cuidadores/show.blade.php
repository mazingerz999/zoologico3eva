@extends('layouts.master')

@section('titulo')
    Zoologico
@endsection

@section('contenido')
<div class="row">
    <div class="col-sm-9">
        <p class="h3">{{$cuidador->nombre}}</p>
       @foreach ($cuidador->titulaciones() as $titulacion)
           <ul>
               <li><a href="{{route('titulaciones.show',$titulacion)}}">Nombre: {{$titulacion->nombre}}</a></li>
           </ul>
        @endforeach

        <div>
            <a href="{{route('animales.index')}}"><button type="button" class="btn btn-secondary">Volver al Listado de Animales</button></a>

    </div>
    </div>
@endsection
