<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="{{url('/assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('/assets/bootstrap/css/estilo.css')}}">
    <title>Zoologico</title>
  </head>
  <body>
    @include('layouts.navbar')

    <div class="container-fluid">

      @yield('contenido')

    </div>


    <script src="{{url('/assets/bootstrap/js/bootstrap.min.js')}}" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

  </body>
</html>

