<?php

use App\Http\Controllers\AnimalController;
use App\Http\Controllers\CuidadorController;
use App\Http\Controllers\InicioController;
use App\Http\Controllers\RevisionController;
use App\Http\Controllers\SoapServerController;
use App\Http\Controllers\TitulacionController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!

|• Añade un middleware que aplique el filtro auth para proteger las rutas de crear y
editar animales.
• Revisa mediante el comando de Artisan php artisan route:list las nuevas rutas
y que el filtro auth se aplique correctamente.
*/


Route::get('/', [InicioController::class, 'home'])->name('home');

Route::get('animales', [AnimalController::class, 'index'])->name('animales.index');

Route::get('animales/create', [AnimalController::class, 'create'])->name('animales.create')->middleware('auth');

Route::get('animales/{animal}', [AnimalController::class, 'show'])->name('animales.show');

Route::get('animales/{animal}/edit', [AnimalController::class, 'edit'])->name('animales.edit')->middleware('auth');

Route::get('revisiones/{animal}/create', [RevisionController::class, 'create'])->name('revisiones.create')->middleware('auth');

Route::post('revisiones/{animal}/create', [RevisionController::class, 'store'])->name('revisiones.store');

Route::get('cuidadores/{cuidador}', [CuidadorController::class, 'show'])->name('cuidadores.show');

Route::get('titulaciones/{titulacion}', [TitulacionController::class, 'show'])->name('titulaciones.show');

Route::post('animales/', [AnimalController::class, 'store'])->name('animales.store');

Route::put('animales/{animal}', [AnimalController::class, 'update'])->name('animales.update');

Route::any('api', [SoapServerController::class, 'getServer']);
Route::any('api/wsdl', [SoapServerController::class, 'getWSDL']);




Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
